# Monitor

#### 介绍
Monitor是针对AGV小车的监控系统 (不含交管策略)  
系统根据 SLAM 构建的概率地图，自定义编辑成实际场景的地图，方便客户展示  
  
系统详细功能：  
    1. 地图编辑  
    2. 物体设置  
    3. 大屏页面  
    4. 系统设置   

#### 软件架构
.net6.0 core webApi + vue3 + RabbitMQ + WebSocket + Fabric.js + element plus


#### 安装教程

1.  nuget 依赖：  
    AutoFac 7.0.1  
    Autofac.Extensions.DependencyInjection 8.0.0  
    GZY.Quartz.MUI 2.4.0  
    Microsoft.AspNetCore.SignalR.Client 7.0.8  
    Microsoft.Windows.Compatibility 7.0.1  
    Newtonsoft.Json 13.0.3  
    RabbitMQ.Client 6.5.0  
    SqlSugarCore 5.1.4.82  
    StackExchange.Redis 2.6.111  
    
2.  node.js v16.14.0
3.  mysql

#### 使用说明

1.  后端运行 Monitor.WebApi  
    初始化数据库：https://localhost:7047/swagger/index.html  
![输入图片说明](image1.png)

2.  前端运行 Monitor.Web  
    cnpm run dev


#### 系统演示

![输入图片说明](image2.png)


#### 源码地址  
https://gitee.com/beanjoe/monitor_src  
需要留下联系方式，加入到源码项目中
